program ReveR;

uses
  Forms,
  RevMain in 'RevMain.pas' {MainForm},
  RegerUnit in 'RegerUnit.pas' {RegForm},
  DatadUnit in 'DatadUnit.pas' {DataForm},
  Reper in 'Reper.pas',
  about in 'about.pas' {AboutBox},
  Rassar in 'Rassar.pas',
  Repernp in 'Repernp.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.Title := 'ReveR';
  Application.CreateForm(TMainForm, MainForm);
  Application.CreateForm(TRegForm, RegForm);
  Application.CreateForm(TDataForm, DataForm);
  Application.CreateForm(TAboutBox, AboutBox);
  Application.Run;
end.
