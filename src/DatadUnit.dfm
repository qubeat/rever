object DataForm: TDataForm
  Left = 299
  Top = 119
  Caption = 'MEM'
  ClientHeight = 360
  ClientWidth = 423
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesigned
  Visible = True
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object MainPanel: TPanel
    Left = 0
    Top = 27
    Width = 423
    Height = 302
    Align = alClient
    TabOrder = 0
    object StackGrid: TDrawGrid
      Left = 293
      Top = 1
      Width = 129
      Height = 300
      Align = alRight
      Color = clBtnFace
      ColCount = 2
      DefaultRowHeight = 20
      RowCount = 16
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Courier New'
      Font.Pitch = fpFixed
      Font.Style = []
      Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine]
      ParentFont = False
      ScrollBars = ssNone
      TabOrder = 0
      OnDrawCell = StackGridDrawCell
      OnGetEditText = StackGridGetEditText
    end
    object DataGrid: TDrawGrid
      Left = 1
      Top = 1
      Width = 292
      Height = 300
      Align = alClient
      Color = clBtnFace
      ColCount = 3
      DefaultRowHeight = 20
      RowCount = 16
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Courier New'
      Font.Style = []
      Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goColSizing, goRowSelect]
      ParentFont = False
      ScrollBars = ssHorizontal
      TabOrder = 1
      OnDblClick = DataGridDblClick
      OnDrawCell = DataGridDrawCell
      OnGetEditText = DataGridGetEditText
      OnMouseDown = DataGridMouseDown
      ColWidths = (
        72
        67
        170)
    end
  end
  object TopPanel: TPanel
    Left = 0
    Top = 0
    Width = 423
    Height = 27
    Align = alTop
    TabOrder = 1
    DesignSize = (
      423
      27)
    object LabelData: TLabel
      Left = 8
      Top = 8
      Width = 38
      Height = 13
      Caption = '&Memory'
      FocusControl = DataGrid
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clHotLight
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object LabelStack: TLabel
      Left = 297
      Top = 8
      Width = 26
      Height = 13
      Anchors = [akTop, akRight]
      Caption = '&Stack'
      FocusControl = StackGrid
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clHotLight
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      ExplicitLeft = 296
    end
    object BevelStack: TBevel
      Left = 292
      Top = 1
      Width = 130
      Height = 25
      Align = alRight
      Shape = bsLeftLine
      Style = bsRaised
      ExplicitLeft = 296
      ExplicitTop = -3
    end
    object LabOffset: TLabel
      Left = 72
      Top = 8
      Width = 35
      Height = 13
      Caption = '&Offset:'
      FocusControl = EditOffset
    end
    object RadioGroupIMP: TRadioGroup
      Left = 200
      Top = 1
      Width = 90
      Height = 30
      Columns = 2
      ItemIndex = 0
      Items.Strings = (
        '&IP'
        '&MP')
      TabOrder = 0
      TabStop = True
      OnClick = RadioGroupIMPClick
    end
    object UpDnOffset: TUpDown
      Left = 185
      Top = 6
      Width = 12
      Height = 21
      Associate = EditOffset
      Min = -32768
      Max = 32767
      TabOrder = 1
      Thousands = False
    end
    object EditOffset: TEdit
      Left = 112
      Top = 6
      Width = 73
      Height = 21
      Color = clBtnFace
      TabOrder = 2
      Text = '0'
      OnChange = EditOffsetChange
    end
  end
  object BotPanel: TPanel
    Left = 0
    Top = 329
    Width = 423
    Height = 31
    Align = alBottom
    TabOrder = 2
    object LabBreak: TLabel
      Left = 8
      Top = 5
      Width = 60
      Height = 13
      Caption = '&Breakpoints:'
      FocusControl = CmBxBreak
    end
    object CmBxBreak: TComboBox
      Left = 74
      Top = 5
      Width = 99
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      Sorted = True
      TabOrder = 0
    end
  end
end
