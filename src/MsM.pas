unit MsM;

interface

uses
  SysUtils,
  Classes;

const
 SepString = ';===';

function RecalcLabs(S : String; dn : Integer) : String;

function NoComments(S : String) : String;

type

 TMLines = class
   NLabs : integer;
   Lines : TStrings;
   constructor Create;
   destructor Destroy; override;
   procedure LoadFromFile(var F : Text);
   procedure SaveToFile(var F : Text);
   procedure JoinWithArgs(Main : TMLines; Args : array of TMLines);
 end;

implementation

function RecalcLabs(S : String; dn : Integer) : String;
var
 n,num : integer;
begin
 Result := '';
 while not (S = '') do
 begin
  n := Pos('@',S);
  if n=0 then
  begin
   Result := Result+S;
   S := ''
  end
  else
  begin
   Result := Result+Copy(S,1,n);
   S := Copy(S,n+1,MaxInt);
   Val(S,num,n);  {won't work with Lazarus!}
   if num <> 0 then num := num+dn;
   if n=0 then S := IntToStr(num)
   else
    if n > 1 then
     S := IntToStr(num)+Copy(S,n,MaxInt);

  end;
 end;
end;

function NoComments(S : String) : String;
var
 n : integer;
begin
 n := Pos(';',S);
 if n > 0 then
 begin
   S := Copy(S,1,n-1);
 end;
 Result := TrimRight(S);
end;

{ TMLines }

constructor TMLines.Create;
begin
 inherited Create;
 NLabs := 0;
 Lines := TStringList.Create;
end;

destructor TMLines.Destroy;
begin
  Lines.Destroy;
  inherited;
end;

procedure TMLines.JoinWithArgs(Main: TMLines; Args: array of TMLines);
var
 N,Cur,i,j,k,er : Integer;
 S : String;
begin
 N := NLabs;
 Cur := N + Main.NLabs;
 for i := 0 to Main.Lines.Count - 1 do
 begin
   S := NoComments(Main.Lines[i]);
   if S <> '' then
   begin
    if S[1] = '$' then
    begin
      Val(Copy(S,2,MaxInt),k,er);
      if (er = 0) and (k < Length(Args)) then
       for j := 0 to Args[k].Lines.Count - 1 do
       begin
        S := NoComments(Args[k].Lines[j]);
        if S <> '' then
        begin
         if Pos('@',S) <> 0 then
         S := RecalcLabs(S,Cur);
         Lines.Append(S);
        end;
       end;
      Cur := Cur+Args[k].NLabs;
    end
    else
    begin
      if Pos('@',S) <> 0 then
       S := RecalcLabs(S,N);
      Lines.Append(S);
    end;
   end;
 end;
 NLabs := Cur;
end;

procedure TMLines.LoadFromFile(var F: Text);
var
 n,Er : integer;
 S : string;
begin
  Lines.Clear;
  nLabs := 0;
  S := '';
  while not Eof(F) do
  begin
    Readln(F,S);
    if (S = SepString) then Exit;
    S := NoComments(S);
    if S <> '' then
    begin
     Lines.Append(S);
     if (S[1] = ':') and (S[2] = '@') then
     begin
       S :=Copy(S,3,Length(S)-2);
       Val(S,n,Er);
       if (Er = 0) and (n > nLabs)
        then nLabs := n
     end;
   end;
  end;
end;


procedure TMLines.SaveToFile(var F: Text);
var
 i : Integer;
begin
 for i := 0 to Lines.Count - 1 do
  writeln(F,Lines[i])
end;

end.
