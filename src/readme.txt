ReveR.exe - simulator
ReveR4.avi - logo
Reper.np - nanoprograms (necessary file)

Use File menu to open program

Buttons

> Step
< Inverse Step
>> Step in nanoprogram
<< Inverse step in nanoprogram

Examples
*.rsr - assembler
*.rmr - memory dump (Attn: should not have empty lines at end of file!)

Format of reper.np

First line of file is number of nanoprograms.
Record with description of any nanoprogram starts with three numbers
n1 n2 n3
n1 - index in array. n2 is number of lines (codes/instructions, see below)
in description of given nanoprogram. n3 is 1 for nanoprogram without
immediate value and 2 for nanoprogram with immediate value.
Name of nanoprogram is in next line. After that there are
n2 lines, each line is code or already described instruction 
and also may use prefix "-" at first position to make inverse
of code or instruction, see comments below for some clarification
(file itself may not have any comments)
----------- Reper.np --------------------
30      <- number of nanoprograms
0 1 1   <- zero index, one line of description, no immediate value 
Nop     <- name
NOP     <- code
3 2 1
Inc RA  <- name
NOT RA  <- code
NEG RA  <- code
4 1 1
Dec RA
-Inc RA <- inverse with prefix "-"
1 3 1
Inc DIP
SWP DIP <- code
Inc RA  <- already described instruction
SWP DIP <- code
------------- ... -----------------------
