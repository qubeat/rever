unit Reper;

interface


uses SysUtils, Classes, Dialogs;


function PMod(A,B : Integer) : Integer;

type

TRegs = (RegCM,RegIP,RegDIP,RegSP,RegMP,RegRA,RegRB,RegRC);

PRPU = ^TRPU;

TROp = class(TObject)  {elementary reversible operation}
  IDX : integer;
  INVIDX : integer;
  OpCode : String;
  constructor Create(anIDX,anINVIDX : Integer);
  procedure perform(PR : PRPU); virtual; abstract;
end;

TNanops = class(TObject)
 IDX : integer;
 Mnemo : String;
 RevCodes : array of integer;
 constructor Create(Num : Integer; Name : String; Codes : array of integer);
 destructor Destroy; override;
 function OpLen : integer; virtual;
end;

TNanops2 = class(TNanops)
 function OpLen : integer; override;
end;

EHistoryError = class(EMathError);

THistory = class(TObject)
 private
  FLen : integer;
  Trash : array of integer;
 public
  constructor Create(Size : Integer);
  destructor Destroy; override;
  procedure Push(const Val : integer); virtual;
  function Pop : Integer; virtual;
  function GetData(i : integer) : integer;
  property Len : integer read FLen;
  //property Top : integer read Pop write Push;
end;


TRPU = class(TObject)     {Reversible Processing Unit}
 RR : array[TRegs] of integer;
 MIM : array of integer;
 NanoProgs : array of TNanops; {Array of nanoprograms}
 RevOps : array of TROp; {Array of ops}
 nIP : Integer; {NanoInstruction Pointer}
 Hist : THistory;
 constructor Create(ATotMem,HistSize : integer);
 destructor Destroy; override;
 procedure InitNanoprogs; virtual;
 function GetMeM(Index: Integer): Integer;
 procedure SetMeM(Index: Integer; const Value: Integer);
 function GetTotMem : integer;
 function GetComm : integer;
 function GetNumOps : integer;
 function GetNumNProgs : integer;
 function FindInstr(Mnm : string; var Inst : integer) : boolean;
 function FindOps(Mnm : string; var nOps : integer) : boolean;
 procedure Step;
 procedure InvStep;
 procedure StartNP(Dir : Boolean);
 procedure FinNP(Dir : Boolean);
 procedure NanoStep;
 procedure InvNanoStep;
 property MeM[Index: Integer]: Integer read GetMeM write SetMeM;
 property TotMem : integer read GetTotMem;
 property Comm : integer read GetComm;
 property NumOps : integer read GetNumOps;
 property NumNProgs : integer read GetNumNProgs;
end;


TRNOP = class(TRop)
  constructor Create;
  procedure perform(PR : PRPU); override;
end;

TRNOTA = class(TRop)
  constructor Create;
  procedure perform(PR : PRPU); override;
end;

TRNEGA = class(TRop)
  constructor Create;
  procedure perform(PR : PRPU); override;
end;

TRSWPB = class(TRop)
  constructor Create;
  procedure perform(PR : PRPU); override;
end;

TRSWPC = class(TRop)
  constructor Create;
  procedure perform(PR : PRPU); override;
end;

TRSWPDIP = class(TRop)
  constructor Create;
  procedure perform(PR : PRPU); override;
end;

TRSWPSP = class(TRop)
  constructor Create;
  procedure perform(PR : PRPU); override;
end;

TRSWPMP = class(TRop)
  constructor Create;
  procedure perform(PR : PRPU); override;
end;


TRSWPMEM = class(TRop)
  constructor Create;
  procedure perform(PR : PRPU); override;
end;

TRNSUB = class(TRop)
  constructor Create;
  procedure perform(PR : PRPU); override;
end;

TRNXSUB = class(TRop)
  constructor Create;
  procedure perform(PR : PRPU); override;
end;


TRNSUBIP = class(TRop)
  constructor Create;
  procedure perform(PR : PRPU); override;
end;

TRCNSUB = class(TRop)
  constructor Create;
  procedure perform(PR : PRPU); override;
end;

TRCNXSUB = class(TRop)
  constructor Create;
  procedure perform(PR : PRPU); override;
end;

TRCLRA = class(TRop)
  constructor Create;
  procedure perform(PR : PRPU); override;
end;

TRUCLA = class(TRop)
  constructor Create;
  procedure perform(PR : PRPU); override;
end;


const

 sRegs : array[TRegs] of string =
  ('CM','IP','DIP','SP','MP','RA','RB','RC');

 OpNOP    = 0;  {NOP}
 OpNOTA =   1;  {RA <- NOT A (-1-RA)}
 OpNEGA =   2;  {RA <- -RA}
 OpSWPB =   3;  {RA <-> RB}
 OpSWPC =   4;  {RA <-> RC}
 OpSWPDIP = 5;  {RA <-> DIP}
 OpSWPSP =  6;  {RA <-> SP}
 OpSWPMP =  7;  {RA <-> MP}
 OpSWPMEM = 8;  {RA <-> MeM[MP]}
 OpNSUB =   9;  {RA <- RB - RA}
 OpNXSUB = 10;  {RA <- [IP+1] - RA}
 OpNSUBIP= 11;  {RA <- IP - RA}
 OpCNSUB = 12;  {IF C > 0 NSUB ELSE NEGA}
 OpCNXSUB= 13;  {IF C > 0 NXSUB ELSE NEGA}
 OpCLRA =  14;  {0 -> RA -> History}
 OpUCLA =  15;  {0 <- RA <- History}


 NumOp = 16;

 sOpCodes : array[0 .. NumOp-1] of string =
  ('NOP', 'NOT RA', 'NEG RA',
   'SWP RB', 'SWP RC', 'SWP DIP',
   'SWP SP', 'SWP MP', 'SWP MEM',
   'NSUB', 'NX SUB', 'NSUB IP',
   'CNSUB', 'CNX SUB',
   'CLRA', 'UCLA');


 NpNop = 0;    {Nop}
 NpIncDIP = 1; {DIP <- DIP + 1}
 NpDecDIP = 2; {DIP <- DIP - 1}

 NpIncA =   3;  {RA <- RA + 1}
 NpDecA =   4;  {RA <- RA - 1}
 NpSwpB =   5;  {RA <-> RB}
 NpSwpC =   6;  {RA <-> RC}
 NpSwpDIP = 7;  {RA <-> DIP}
 NpSwpSP =  8;  {RA <-> SP}
 NpSwpMP =  9;  {RA <-> MP}
 NpSwpMEM =10;  {RA <-> MeM[MP]}
 NpAdd =   11;  {RA <- RA + RB}
 NpSub =   12;  {RA <- RA - RB}
 NpAddNX = 13;  {RA <- RA + [IP+1]}
 NpSubNX = 14;  {RA <- RA - [IP+1]}
 NpAddIP = 15;  {RA <- RA + IP}
 NpSubIP = 16;  {RA <- RA - IP}
 NpNegA =  17;  {RA <- -RA}

 NpJmpdNx = 18;  {DIP <- DIP + [IP+1]}
 NpSubr = 19;  {DIP <-> [SP]; INC DIP; DEC SP}
 NpRet = 20; {INC SP; DEC DIP; DIP <-> [SP]; DIP <- [IP+1]-DIP}
 NpCJmpdNx = 21;  {IF C > 0 THEN DIP <- DIP + [IP+1]}
 NpCAdd =   22;  {IF C > 0 THEN RA <- RA + RB}
 NpPushA =  23;  {RA <-> [SP]; DEC SP}
 NpPopA =   24;  {INC SP; RA <-> [SP] }
 NpClearA = 25;  {Clear RA}

 NumNanoPr = 26;


implementation


function PMod(A,B : Integer) : Integer;
begin
  if (A >= 0) and (A < B) then
   Result := A
  else
  begin
   Result := A mod B;
   if Result < 0 then Result := Result + B
  end;
end;


{ RCPU }

constructor TRPU.Create(ATotMem,HistSize : integer);
var
 i : integer;
begin
  inherited Create;
  SetLength(MIM,ATotMem);
  RR[RegCM] := 0;  {0-CM}
  RR[RegIP] := 0;  {1-IP}
  RR[RegDIP] := 0; {2-DIP}
  RR[RegSP] := ATotMem-1;{3-SP}
  RR[RegMP] := 0;  {4-MP}
  RR[RegRA] := 0;  {5-RA}
  RR[RegRB] := 0;  {6-RB}
  RR[RegRC] := 0;  {7-RC}
  nIP := 0;
  Hist := THistory.Create(HistSize);
  SetLength(RevOps,NumOp);
  RevOps[OpNOP] := TRNOP.Create;
  RevOps[OpNEGA] := TRNEGA.Create;
  RevOps[OpNOTA] := TRNOTA.Create;
  RevOps[OpSWPB] := TRSWPB.Create;
  RevOps[OpSWPC] := TRSWPC.Create;
  RevOps[OpSWPDIP] := TRSWPDIP.Create;
  RevOps[OpSWPSP] := TRSWPSP.Create;
  RevOps[OpSWPMP] := TRSWPMP.Create;
  RevOps[OpSWPMEM] := TRSWPMEM.Create;
  RevOps[OpNSUB] := TRNSUB.Create;
  RevOps[OpNXSUB] := TRNXSUB.Create;
  RevOps[OpNSUBIP] := TRNSUBIP.Create;
  RevOps[OpCNSUB] := TRCNSUB.Create;
  RevOps[OpCNXSUB] := TRCNXSUB.Create;
  RevOps[OpCLRA] := TRCLRA.Create;
  RevOps[OpUCLA] := TRUCLA.Create;
  for i := 0 to Length(RevOps) - 1 do
    if (RevOps[i].IDX <> i) or
      (RevOps[RevOps[i].INVIDX].INVIDX <> i)
      then ShowMessage('Error in opcodes initialization');

  InitNanoprogs;

end;

destructor TRPU.Destroy;
var
 i : integer;
begin
  for i := 0 to Length(NanoProgs)-1 do
    NanoProgs[i].Free;
  NanoProgs := nil;
  for i := 0 to Length(RevOps)-1 do
    RevOps[i].Free;
  RevOps := nil;
  Hist.Free;
  inherited;
end;

function TRPU.GetComm: integer;
begin
 Result := RR[RegCM];
 if (Result < 0) or (Result >= Length(NanoProgs)) then
  Result := 0
end;

function TRPU.GetMeM(Index: Integer): Integer;
begin
 GetMeM := MIM[PMod(Index,Length(MIM))];
end;

procedure TRPU.SetMeM(Index: Integer; const Value: Integer);
begin
 MIM[PMod(Index,Length(MIM))] := Value;
end;

function TRPU.GetNumNProgs: integer;
begin
  GetNumNProgs := Length(NanoProgs);
end;

function TRPU.GetNumOps: integer;
begin
 GetNumOps := Length(RevOps)
end;

function TRPU.GetTotMem: integer;
begin
 GetTotMem := Length(MIM)
end;

procedure TRPU.Step;
var
 i : integer;
begin
 if nIP <> 0 then Exit;
 StartNP(True);
 try
  with NanoProgs[Comm] do
  for i := 0 to Length(RevCodes) - 1 do
   RevOps[RevCodes[i]].perform(@Self);
 finally
  FinNP(True);
 end;
 
end;

procedure TRPU.InvStep;
var
 i : integer;
begin
 if nIP <> 0 then Exit;
 FinNP(False);
 try
  with NanoProgs[Comm] do
  for i := Length(RevCodes) - 1 downto 0 do
   RevOps[RevOps[RevCodes[i]].INVIDX].perform(@Self);
 finally
  StartNP(False);
 end;
end;


procedure TRPU.NanoStep;
begin
  if nIP = 0 then
  begin
    StartNP(True);
    nIP := 1
  end
  else
   with NanoProgs[Comm] do
   if nIP <= Length(RevCodes) then
   begin
    RevOps[RevOps[RevCodes[nIP-1]].INVIDX].perform(@Self);
    inc(nIP)
   end
    else
    begin
     FinNP(True);
     nIP := 0;
    end;
end;

procedure TRPU.InitNanoprogs;
var
 i : integer;
begin
  SetLength(NanoProgs,NumNanoPr);
  NanoProgs[NpNop] := TNanops.Create(NpNop,'Nop',[OpNOP]);
  NanoProgs[NpIncDIP] := TNanops.Create(NpIncDIP,
             'Inc DIP',[OpSWPDIP,OpNOTA,OpNEGA,OpSWPDIP]);
  NanoProgs[NpDecDIP] := TNanops.Create(NpDecDIP,
             'Dec DIP',[OpSWPDIP,OpNEGA,OpNOTA,OpSWPDIP]);
  NanoProgs[NpIncA] := TNanops.Create(NpIncA,
             'Inc RA',[OpNOTA,OpNEGA]);
  NanoProgs[NpDecA] := TNanops.Create(NpDecA,
             'Dec RA',[OpNEGA,OpNOTA]);
  NanoProgs[NpSwpB] := TNanops.Create(NpSwpB,
             'Swap RA,RB',[OpSWPB]);
  NanoProgs[NpSwpC] := TNanops.Create(NpSwpC,
             'Swap RA,RC',[OpSWPC]);
  NanoProgs[NpSwpDIP] := TNanops.Create(NpSwpDIP,
             'Swap RA,DIP',[OpSWPDIP]);
  NanoProgs[NpSwpSP] := TNanops.Create(NpSwpSP,
             'Swap RA,SP',[OpSWPSP]);
  NanoProgs[NpSwpMP] := TNanops.Create(NpSwpMP,
             'Swap RA,MP',[OpSWPMP]);
  NanoProgs[NpSwpMEM] := TNanops.Create(NpSwpMEM,
             'Swap RA,[MP]',[OpSWPMEM]);
  NanoProgs[NpAdd] := TNanops.Create(NpAdd,
             'Add RA,RB',[OpNEGA,OpNSUB]);
  NanoProgs[NpSub] := TNanops.Create(NpSub,
             'Sub RA,RB',[OpNSUB,OpNEGA]);
  NanoProgs[NpAddNX] := TNanops2.Create(NpAddNX,
             'Add RA,[IP+1]',[OpNEGA,OpNXSUB]);
  NanoProgs[NpSubNX] := TNanops2.Create(NpSubNX,
             'Sub RA,[IP+1]',[OpNXSUB,OpNEGA]);
  NanoProgs[NpAddIP] := TNanops.Create(NpAddIP,
             'Add RA,IP',[OpNEGA,OpNSUBIP]);
  NanoProgs[NpSubIP] := TNanops.Create(NpSubIP,
             'Sub RA,IP',[OpNSUBIP,OpNEGA]);
  NanoProgs[NpNegA] := TNanops.Create(NpNegA,
             'Neg RA',[OpNEGA]);
  NanoProgs[NpJmpdNx] := TNanops2.Create(NpJmpdNx,
             'Jmp dNx',[OpSWPDIP,OpNEGA,OpNXSUB,OpSWPDIP]);
  NanoProgs[NpSubr] := TNanops.Create(NpSubr,
       'Subr',
       [OpSWPSP,OpSWPMP,OpSWPSP, {Swap MP,SP}
        OpSWPDIP,OpSWPMEM,OpNOTA,OpNEGA,OpSWPDIP, {Swap DIP,[MP]; Inc DIP}
        OpSWPSP,OpSWPMP,{OpSWPSP,} {Swap MP,SP}
        {OpSWPSP,}OpNEGA,OpNOTA,OpSWPSP {Dec SP}
               ]);
  NanoProgs[NpRet] := TNanops2.Create(NpRet,
       'Ret',
        [OpSWPSP,OpNOTA,OpNEGA, {OpSWPSP,} {INC SP}
        {OpSWPSP,}OpSWPMP,OpSWPSP, {Swap MP,SP}
        OpSWPDIP,  {DIP <-> RA}
        OpNEGA,OpNOTA, {DEC RA}
        OpSWPMEM,  {RA <-> [MP]}
        OpSWPSP,OpSWPMP,OpSWPSP, {Swap MP,SP}
        OpNXSUB, {RA <- [IP+1]-RA}
        OpSWPDIP]);  {DIP <-> RA}
  NanoProgs[NpCJmpdNx] := TNanops2.Create(NpCJmpdNx,
             'CJmp dNx',[OpSWPDIP,OpNEGA,OpCNXSUB,OpSWPDIP]);
  NanoProgs[NpCAdd] := TNanops.Create(NpCAdd,
             'CAdd RA,RB',[OpNEGA,OpCNSUB]);
  NanoProgs[NpPushA] := TNanops.Create(NpPushA,
       'Push RA',
       [OpSWPSP,OpSWPMP,OpSWPSP, {Swap MP,SP}
        OpSWPMEM, {Swap RA,[MP]}
        OpSWPSP,OpSWPMP,{OpSWPSP,} {Swap MP,SP}
        {OpSWPSP,}OpNEGA,OpNOTA,OpSWPSP {Dec SP}
               ]);
  NanoProgs[NpPopA] := TNanops.Create(NpPopA,   {Inv NpPushA}
       'Pop RA',
       [OpSWPSP,OpNOTA,OpNEGA,{OpSWPSP,} {Inc SP}
        {OpSWPSP,}OpSWPMP,OpSWPSP, {Swap MP,SP}
        OpSWPMEM, {Swap RA,[MP]}
        OpSWPSP,OpSWPMP,OpSWPSP {Swap MP,SP}
               ]);
  NanoProgs[NpClearA] := TNanops.Create(NpClearA,
             'Clear RA',[OpCLRA]);
  for i := 0 to Length(NanoProgs) - 1 do
   if NanoProgs[i].IDX <> i
      then ShowMessage('Error in nanoprogs initialization');
end;

procedure TRPU.InvNanoStep;
begin
 if nIP = 0 then
 begin
   FinNP(False);
   nIP := Length(NanoProgs[Comm].RevCodes)+1
 end
 else
  if nIP = 1 then
  begin
     StartNP(False);
     nIP := 0
  end
  else
   with NanoProgs[Comm] do
   begin
      dec(nIP);
      RevOps[RevOps[RevCodes[nIP-1]].INVIDX].perform(@Self);
   end;
end;

procedure TRPU.StartNP(Dir: Boolean);
begin
 If Dir then RR[RegCM] := RR[RegCM] + MeM[RR[RegIP]]
 else RR[RegCM] := RR[RegCM] - MeM[RR[RegIP]];
end;

function TRPU.FindInstr(Mnm: string; var Inst: integer): boolean;
begin
 result := false;
 Inst := 0;
 while (Inst < Length(NanoProgs))
   and ((NanoProgs[Inst] = nil)
    or (NanoProgs[Inst].Mnemo <> Mnm))
   do Inc(Inst);
 result := (Inst < NumNProgs)
end;

function TRPU.FindOps(Mnm: string; var nOps: integer): boolean;
begin
 result := false;
 nOps := 0;
 while (nOps < Length(RevOps))
   and (RevOps[nOps].OpCode <> Mnm)
   do Inc(nOps);
 result := (nOps < NumOps)
end;

procedure TRPU.FinNP(Dir: Boolean);
begin
 If Dir then
 begin
  RR[RegCM] := RR[RegCM] - MeM[RR[RegIP]];
  RR[RegIP] := RR[RegIP] + RR[RegDIP];
 end
 else
 begin
  RR[RegIP] := RR[RegIP] - RR[RegDIP];
  RR[RegCM] := RR[RegCM] + MeM[RR[RegIP]];
 end;
end;

{ TNanops }

constructor TNanops.Create(Num: Integer; Name: String; Codes: array of integer);
var
 i : integer;
begin
 inherited Create;
 IDX := Num;
 Mnemo := Name;
 SetLength(RevCodes,Length(Codes));
 for i := 0 to Length(Codes) - 1 do
   RevCodes[i] := Codes[i]
end;

destructor TNanops.Destroy;
begin
  RevCodes := nil;
  inherited;
end;

function TNanops.OpLen: integer;
begin
 OpLen := 1;
end;

{ TNanops2 }

function TNanops2.OpLen: integer;
begin
 OpLen := 2;
end;

{ TROp }

constructor TROp.Create(anIDX, anINVIDX: Integer);
begin
  inherited Create;
  IDX := anIDX;
  INVIDX := anINVIDX;
  OpCode := sOpCodes[IDX]
end;


{ TRNOP }

constructor TRNOP.Create;
begin
 inherited Create(OpNOP,OpNOP);
end;

procedure TRNOP.perform(PR : PRPU);
begin
 {NO Operation}
end;

{ TRNEGA }

constructor TRNEGA.Create;
begin
 inherited Create(OpNEGA,OpNEGA)
end;

procedure TRNEGA.perform(PR: PRPU);
begin
 with PR^ do
  RR[RegRA] := -RR[RegRA]
 { RA <- -RA }
end;


{ TRNOTA }

constructor TRNOTA.Create;
begin
 inherited Create(OpNOTA,OpNOTA)
end;

procedure TRNOTA.perform(PR : PRPU);
begin
 with PR^ do
  RR[RegRA] := -1 - RR[RegRA]
 {RA <- - 1 - RA}
end;

{ TRSWPB }

constructor TRSWPB.Create;
begin
 inherited Create(OpSWPB,OpSWPB)
end;

procedure TRSWPB.perform(PR: PRPU);
var
 M : integer;
begin
 with PR^ do
 begin
  M := RR[RegRA];
  RR[RegRA] := RR[RegRB];
  RR[RegRB] := M
 end;
 {RA <-> RB}
end;

{ TRSWPC }

constructor TRSWPC.Create;
begin
 inherited Create(OpSWPC,OpSWPC)
end;

procedure TRSWPC.perform(PR: PRPU);
var
 M : integer;
begin
 with PR^ do
 begin
  M := RR[RegRA];
  RR[RegRA] := RR[RegRC];
  RR[RegRC] := M
 end;
 {RA <-> RC}
end;

{ TRSWPDIP }

constructor TRSWPDIP.Create;
begin
 inherited Create(OpSWPDIP,OpSWPDIP)
end;

procedure TRSWPDIP.perform(PR: PRPU);
var
 M : integer;
begin
 with PR^ do
 begin
  M := RR[RegRA];
  RR[RegRA] := RR[RegDIP];
  RR[RegDIP] := M
 end;
 {RA <-> DIP}
end;

{ TSWPSP }

constructor TRSWPSP.Create;
begin
 inherited Create(OpSWPSP,OpSWPSP)
end;

procedure TRSWPSP.perform(PR: PRPU);
var
 M : integer;
begin
 with PR^ do
 begin
  M := RR[RegRA];
  RR[RegRA] := RR[RegSP];
  RR[RegSP] := M
 end;
 {RA <-> SP}
end;

{ TSWPMP }

constructor TRSWPMP.Create;
begin
 inherited Create(OpSWPMP,OpSWPMP)
end;

procedure TRSWPMP.perform(PR: PRPU);
var
 M : integer;
begin
 with PR^ do
 begin
  M := RR[RegRA];
  RR[RegRA] := RR[RegMP];
  RR[RegMP] := M
 end;
 {RA <-> MP}
end;


{ TRSWPMEM }

constructor TRSWPMEM.Create;
begin
 inherited Create(OpSWPMEM,OpSWPMEM)
end;

procedure TRSWPMEM.perform(PR: PRPU);
var
 M : integer;
begin
 with PR^ do
 begin
  M := RR[RegRA];
  RR[RegRA] := MeM[RR[RegMP]];
  MeM[RR[RegMP]] := M
 end;
 {RA <-> MeM[MP]}
end;


{ TRNSUB }

constructor TRNSUB.Create;
begin
 inherited Create(OpNSUB,OpNSUB)
end;

procedure TRNSUB.perform(PR: PRPU);
begin
 with PR^ do
  RR[RegRA] := RR[RegRB] - RR[RegRA]
 { RA <- RB - RA }
end;

{ TRSUBNX }

constructor TRNXSUB.Create;
begin
 inherited Create(OpNXSUB,OpNXSUB)
end;

procedure TRNXSUB.perform(PR: PRPU);
begin
 with PR^ do
  RR[RegRA] := MeM[RR[RegIP]+1]- RR[RegRA]
 { RA <- MeM[IP+1] - RA }
end;

{ TRNSUBIP }

constructor TRNSUBIP.Create;
begin
 inherited Create(OpNSUBIP,OpNSUBIP)
end;

procedure TRNSUBIP.perform(PR: PRPU);
begin
with PR^ do
  RR[RegRA] := RR[RegIP] - RR[RegRA]
 { RA <- IP - RA }
end;

{ TRCNSUB }

constructor TRCNSUB.Create;
begin
 inherited Create(OpCNSUB,OpCNSUB)
end;

procedure TRCNSUB.perform(PR: PRPU);
begin
 with PR^ do
 if RR[RegRC] > 0 then
  RR[RegRA] := RR[RegRB] - RR[RegRA]
 else
  RR[RegRA] := -RR[RegRA]
 { IF RC > 0 THEN RA <- RB - RA ELSE RA <- -RA}
end;

{ TRCNXSUB }

constructor TRCNXSUB.Create;
begin
 inherited Create(OpCNXSUB,OpCNXSUB)
end;

procedure TRCNXSUB.perform(PR: PRPU);
begin
 with PR^ do
 if RR[RegRC] > 0 then
  RR[RegRA] := MeM[RR[RegIP]+1]- RR[RegRA]
 else
  RR[RegRA] := -RR[RegRA]
 { IF RC > 0 THEN RA <- MeM[IP+1] - RA ELSE RA <- -RA }
end;


{ THistory }

constructor THistory.Create(Size: Integer);
begin
 inherited Create;
 FLen := 0;
 SetLength(Trash,Size);
end;

destructor THistory.Destroy;
begin
 Trash := nil;
 inherited;
end;

function THistory.GetData(i: integer): integer;
begin
 if (i >=0) and (i < FLen) then GetData := Trash[FLen-i]
 else GetData := 0
end;

function THistory.Pop: Integer;
begin
 Result := 0;
 If FLen <= 0 then
  raise EHistoryError.Create('Empty history')
 else
 begin
  Dec(FLen);
  Result := Trash[FLen];
 end;
end;

procedure THistory.Push(const Val: integer);
begin
 If FLen >= Length(Trash) then
  raise EHistoryError.Create('History overflow')
 else
 begin
  Trash[FLen] := Val;
  Inc(FLen);
 end;
end;

{ TRCLRA }

constructor TRCLRA.Create;
begin
 inherited Create(OpCLRA,OpUCLA)
end;

procedure TRCLRA.perform(PR: PRPU);
begin
 with PR^ do
 begin
   Hist.Push(RR[RegRA]);
   RR[RegRA] := 0;
 end;
end;

{ TRUCLA }

constructor TRUCLA.Create;
begin
 inherited Create(OpUCLA,OpCLRA)
end;

procedure TRUCLA.perform(PR: PRPU);
begin
 with PR^ do
 if RR[RegRA] <> 0 then
 raise
   EHistoryError.Create('Error: restoring nonzero RA')
 else
   RR[RegRA] := Hist.Pop;
end;

end.
