unit DatadUnit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, ExtCtrls, StdCtrls, ComCtrls;

type
  CellFlags = (cfCurrent,cfBreak);
  CellKind = Set of CellFlags;

  TChgFlag = (cfFalse,cfTrue,cfToggle);

  TDataForm = class(TForm)
    MainPanel: TPanel;
    StackGrid: TDrawGrid;
    DataGrid: TDrawGrid;
    TopPanel: TPanel;
    LabelData: TLabel;
    LabelStack: TLabel;
    BevelStack: TBevel;
    RadioGroupIMP: TRadioGroup;
    UpDnOffset: TUpDown;
    EditOffset: TEdit;
    LabOffset: TLabel;
    BotPanel: TPanel;
    CmBxBreak: TComboBox;
    LabBreak: TLabel;
    procedure DataGridDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure DataGridGetEditText(Sender: TObject; ACol, ARow: Integer;
      var Value: string);
    procedure StackGridDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure StackGridGetEditText(Sender: TObject; ACol, ARow: Integer;
      var Value: string);
    procedure RadioGroupIMPClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure EditOffsetChange(Sender: TObject);
    procedure DataGridMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure DataGridDblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
   DataGridPoint : TPoint; 
   procedure DataGridGetData(ACol, ARow: Integer;
    var Value: string; var CK : CellKind);
   procedure StackGridGetData(ACol, ARow: Integer;
    var Value: string);
   function RowToMem(ARow : Integer) : Integer;
   function DataOffset : integer;
   function CheckBrkAddr(Idx : Integer) : boolean;
   function SetBrkAddr(Idx : Integer; Op : TChgFlag) : Integer;
   procedure ClearBrk;
    { TODO -oAlex -cGrids : Grids editors }
  end;

var
  DataForm: TDataForm;

implementation

{$R *.dfm}

uses Reper, RegerUnit;

procedure TDataForm.ClearBrk;
begin
 CmBxBreak.Clear;
end;

procedure TDataForm.DataGridDblClick(Sender: TObject);
var
 ACol,ARow : Integer;
begin
 with DataGridPoint do
  DataGrid.MouseToCell(X,Y,ACol,ARow);
 if (ACol=0) and (ARow > 0) then
 begin
  CmBxBreak.ItemIndex:=-1; 
  CmBxBreak.ItemIndex:=SetBrkAddr(RowToMem(ARow),cfToggle);
  DataGrid.Repaint;
 end;

end;

procedure TDataForm.DataGridDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
var
 S : String;
 CK : CellKind;
begin
  DataGridGetData(ACol,ARow,S,CK);
  with DataGrid.Canvas do
  begin
   if (ACol = 0) and (ARow > 0) then
    Font.Color := LabelData.Font.Color;
   TextOut(Rect.Left+2, Rect.Top+2,S);
   if (cfBreak in CK) then
   begin
     Pen.Color := clBtnShadow;
     Brush.Style := bsSolid;
     Brush.Color := clRed;
     Ellipse(Rect.Right-8,Rect.Top+1,
        Rect.Right-1,Rect.Top+8);
   end;

   if (cfCurrent in CK) then
   begin
    Brush.Style := bsSolid;
    Brush.Color := clBtnShadow;
    Pen.Color := DataGrid.Font.Color;
    Polygon([Point(Rect.Right-2, (Rect.Top+Rect.Bottom) div 2),
     Point(Rect.Right-7, (Rect.Top+Rect.Bottom) div 2 - 4),
     Point(Rect.Right-7, (Rect.Top+Rect.Bottom) div 2 + 4)])
   end;
  end;


end;

procedure TDataForm.DataGridGetData(ACol, ARow: Integer; var Value: string;
 var CK : CellKind);
var
 Idx,Com : integer;
begin
  CK := [];
  Idx := RowToMem(ARow);
  case ACol of
   0 :
   begin
    if ARow = 0 then value := 'Address' else
    begin
     value := IntToHex(Idx,8);
     if Idx = RegForm.RCPU.RR[RegIP] then CK := CK + [cfCurrent];
     if CheckBrkAddr(Idx) then CK := CK + [cfBreak];
    end;
   end;
   1 :
    begin
     if ARow = 0 then value :='Data' else
       value := IntToHex(RegForm.RCPU.Mem[Idx],8);
    
    end;
   2 :
    begin
     if ARow = 0 then value := 'Command' else
     with  RegForm.RCPU do
     begin
       Com := Mem[Idx];
       if (Com >= 0) and (Com < Length(NanoProgs)) then
       begin
        value := NanoProgs[Com].Mnemo;
        if NanoProgs[Com].OpLen = 2 then
        value := value + ' ('+IntToStr(Mem[Idx+1])+')'
       end
       else
        value := '0x'+IntToHex(Com,1)
     end
    end;
  end;
end;

procedure TDataForm.DataGridGetEditText(Sender: TObject; ACol, ARow: Integer;
  var Value: string);
var
 CK : CellKind;
begin
 DataGridGetData(ACol,ARow,Value,CK)
end;

procedure TDataForm.DataGridMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  DataGridPoint.X := X;
  DataGridPoint.Y := Y;
end;

function TDataForm.DataOffset: integer;
begin
 DataOffset := UpDnOffset.Position
end;

procedure TDataForm.EditOffsetChange(Sender: TObject);
begin
 DataGrid.Repaint
end;

procedure TDataForm.FormResize(Sender: TObject);
const
 MaxRow = 100;
begin
 with DataGrid do
 begin
  RowCount := MaxRow;
  RowCount := VisibleRowCount + 2;
 end;
 with StackGrid do
 begin
  RowCount := MaxRow;
  RowCount := VisibleRowCount + 2;
 end;
end;

procedure TDataForm.RadioGroupIMPClick(Sender: TObject);
begin
 DataGrid.Repaint
end;

function TDataForm.RowToMem(ARow: Integer): Integer;
var
 Idx : Integer;
begin
  if RadioGroupIMP.ItemIndex = 0 then
   Idx := RegForm.RCPU.RR[RegIP]+ARow-1
  else
   Idx := RegForm.RCPU.RR[RegMP]+ARow-1;
  Idx := Idx + DataOffset;
  Result := PMod(Idx,RegForm.RCPU.TotMem);
end;

procedure TDataForm.StackGridDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
var
 S : String;
begin
 StackGridGetData(ACol,ARow,S);
 with StackGrid.Canvas do
 begin
  if (ACol = 0) and (ARow > 0) then
    Font.Color := LabelStack.Font.Color;
  TextOut(Rect.Left+2, Rect.Top+2,S);
 end;

end;

procedure TDataForm.StackGridGetData(ACol, ARow: Integer; var Value: string);
var
 Idx : integer;
begin
  Idx := RegForm.RCPU.RR[RegSP]+ARow-2;
  case ACol of
   0 :
   begin
    if ARow = 0 then value := 'Pointer' else
    if (Idx >= 0) and (Idx < RegForm.RCPU.TotMem) then
     value := Format('%4d+SP',[ARow-2])
    else
     value := '';
   end;
   1 :
    begin
     if ARow = 0 then value :='Data' else
      if (Idx >= 0) and (Idx < RegForm.RCPU.TotMem) then
       value := IntToHex(RegForm.RCPU.Mem[Idx],8)
      else
       value := '' ;
    end;
  end;
end;


procedure TDataForm.StackGridGetEditText(Sender: TObject; ACol, ARow: Integer;
  var Value: string);
begin
 StackGridGetData(ACol,ARow,Value)
end;

function TDataForm.CheckBrkAddr(Idx: Integer): boolean;
var
 SAddr : string;
begin
 SAddr := IntToHex(Idx,8);
 Result := (CmBxBreak.Items.IndexOf(SAddr) <> -1)
end;


function TDataForm.SetBrkAddr(Idx : Integer; Op : TChgFlag) : Integer;
var
 SAddr : string;
 It : integer;
begin
 Result := -1;
 SAddr := IntToHex(Idx,8);
 with CmBxBreak.Items do
 begin
  It := IndexOf(SAddr);
  if It = -1 then
   if Op in [cfTrue,cfToggle] then Result := Add(SAddr)
   else Result := It
  else
   if Op in [cfFalse,cfToggle] then Delete(It)
   else Result := It;
 end;

end;

end.
