unit Repernp;

interface

uses SysUtils, Classes, Dialogs, Reper;

type

 TRPUNP = Class(TRPU)
  constructor Create(ATotMem,HistSize : integer; NPFileName : String);
  procedure InitNanoprogs; override;
  procedure LoadNanoprogs(NPFileName : String); virtual;
  procedure LoadNanoprog(var F : TextFile); virtual;
 end;



implementation

{ TRPUNP }

constructor TRPUNP.Create(ATotMem, HistSize: integer; NPFileName: String);
begin
  inherited Create(ATotMem,HistSize);
  LoadNanoprogs(NPFileName);
end;

procedure TRPUNP.InitNanoprogs;
begin
 {Do nothing}
end;

procedure TRPUNP.LoadNanoprog(var F: TextFile);
const
 MaxOps = 4096;
var
 TmpCodes : array[0..MaxOps-1] of integer;
 i,j,l,n,tp,ct,op : integer;
 NPName,OPName : string;
 Codes : array of integer;
 procedure AddOp(op : integer);
 begin
  if ct = 0 then
  begin
   TmpCodes[ct] := op;
   inc(ct)
  end
  else
   if op <> 0 then
    if RevOps[op].INVIDX = TmpCodes[ct-1]
    then dec(ct)
    else
    begin
     TmpCodes[ct] := op;
     inc(ct)
    end
 end;
begin
 Readln(F,n,l,tp);
 Readln(F,NPName);
 NPName := Trim(NPName);
 ct := 0;
 for i := 1 to l do
 begin
   Readln(F,OPName);
   OPName := Trim(OPName);
   if OPName[1] <> '-' then
   begin
    if FindOps(OPName,op) then AddOp(op)
    else
     if FindInstr(OPName,op) then
     with NanoProgs[op] do
      for j := 0 to Length(RevCodes) - 1 do
       AddOp(RevCodes[j])
     else
      raise EInvalidOp.Create('Invalid Op: '+OPName);
    end
    else
    begin
     OPName := Copy(OPName,2,Length(OpName)-1);
     if FindOps(OPName,op) then
      AddOP(RevOps[op].INVIDX)
     else
      if FindInstr(OPName,op) then
      with NanoProgs[op] do
       for j := Length(RevCodes) - 1 downto 0  do
        AddOp(RevOps[RevCodes[j]].INVIDX)
      else
       raise EInvalidOp.Create('Invalid Op: '+OPName);
    end;
    SetLength(Codes,ct);
    for j := 0 to ct - 1 do
     Codes[j] := TmpCodes[j];
    case tp of
     1 : NanoProgs[n] := TNanops.Create(n,NPName,Codes);
     2 : NanoProgs[n] := TNanops2.Create(n,NPName,Codes);
    end;
 end;

end;

procedure TRPUNP.LoadNanoprogs(NPFileName: String);
var
 F : TextFile;
 num : integer;
begin
 try
   AssignFile(F,NPFileName);
   Reset(F);
   Readln(F,num);
   SetLength(NanoProgs,num);
   while not Eof(F) do
   begin
     LoadNanoprog(F);
   end;
 finally
   Close(F)
 end;
end;

end.
