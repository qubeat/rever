unit RevMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, ActnList, StdActns, ComCtrls;

type
  TMainForm = class(TForm)
    MainMenu1: TMainMenu;
    Debug1: TMenuItem;
    File1: TMenuItem;
    Exit1: TMenuItem;
    N1: TMenuItem;
    PrintSetup1: TMenuItem;
    Print1: TMenuItem;
    N2: TMenuItem;
    SaveAs1: TMenuItem;
    Save1: TMenuItem;
    Open1: TMenuItem;
    New1: TMenuItem;
    Registers1: TMenuItem;
    ActionList1: TActionList;
    FileExit1: TFileExit;
    ActRegisters: TAction;
    Data1: TMenuItem;
    ActData: TAction;
    Help1: TMenuItem;
    About1: TMenuItem;
    HelpAbout1: TAction;
    FileOpen1: TFileOpen;
    FileSaveAs1: TFileSaveAs;
    FileNew1: TAction;
    AnimateLogo: TAnimate;
    procedure Debug1Click(Sender: TObject);
    procedure ActRegistersExecute(Sender: TObject);
    procedure ActDataExecute(Sender: TObject);
    procedure HelpAbout1Execute(Sender: TObject);
    procedure FileOpen1Accept(Sender: TObject);
    procedure FileSaveAs1Accept(Sender: TObject);
    procedure FileNew1Execute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure OpenMemDump(FileName : TFileName);
    procedure OpenRAsm(FileName: TFileName);
    procedure SaveMemDump(FileName : TFileName);
  end;

var
  MainForm: TMainForm;

implementation

{$R *.dfm}

uses RegerUnit, DatadUnit, About, Reper, Rassar;

procedure TMainForm.ActDataExecute(Sender: TObject);
begin
 DataForm.Visible :=  ActData.Checked
end;


procedure TMainForm.ActRegistersExecute(Sender: TObject);
begin
 RegForm.Visible :=  ActRegisters.Checked
end;

procedure TMainForm.Debug1Click(Sender: TObject);
begin
 ActRegisters.Checked := RegForm.Visible;
 ActData.Checked := DataForm.Visible
end;

procedure TMainForm.FileOpen1Accept(Sender: TObject);
begin
 with FileOpen1.Dialog do
 case FilterIndex of
   1 : OpenMemDump(FileName);
   2 : OpenRasm(FileName);
 end;
end;

procedure TMainForm.FileSaveAs1Accept(Sender: TObject);
begin
  with FileSaveAs1.Dialog do
    SaveMemDump(FileName);
end;

procedure TMainForm.HelpAbout1Execute(Sender: TObject);
begin
 AboutBox.ShowModal
end;

procedure TMainForm.FileNew1Execute(Sender: TObject);
var
 i : integer;
begin
 with RegForm.RCPU do
 for i := 0 to TotMeM - 1 do
  MeM[i] := 0;
 DataForm.ClearBrk; 
 DataForm.DataGrid.Repaint;
 DataForm.StackGrid.Repaint;
end;

procedure TMainForm.OpenMemDump(FileName: TFileName);
var
 F : TextFile;
 Addr,Dat : integer;
begin
 try
  AssignFile(F,FileName);
  Reset(F);
  while not EOF(F) do
  begin
   Readln(F,Addr,Dat);
   with RegForm.RCPU do
    MeM[Addr] := Dat;
  end;
 finally
  CloseFile(F);
  DataForm.DataGrid.Repaint;
  DataForm.StackGrid.Repaint;
 end;
end;

procedure TMainForm.OpenRAsm(FileName: TFileName);
var
 CP0 : Integer;
begin
  CP0 := 0;
  LoadRasm(FileName,RegForm.RCPU,CP0);
  DataForm.DataGrid.Repaint;
  DataForm.StackGrid.Repaint;
end;

procedure TMainForm.SaveMemDump(FileName: TFileName);
var
 F : TextFile;
 i : integer;
begin
 try
  AssignFile(F,FileName);
  Rewrite(F);
  with RegForm.RCPU do
    for i := 0 to TotMeM - 1 do
     if MeM[i] <> 0 then Writeln(F,i,' ',MeM[i])
 finally
  CloseFile(F);
 end;
end;
end.
