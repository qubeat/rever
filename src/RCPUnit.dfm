object RegForm1: TRegForm1
  Left = 8
  Top = 135
  BorderStyle = bsToolWindow
  Caption = 'Registers'
  ClientHeight = 166
  ClientWidth = 265
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesigned
  PixelsPerInch = 96
  TextHeight = 13
  object MainPanel: TPanel
    Left = 0
    Top = 0
    Width = 265
    Height = 166
    Align = alClient
    BevelOuter = bvLowered
    TabOrder = 0
    object LabelIP: TLabel
      Left = 8
      Top = 11
      Width = 14
      Height = 13
      Caption = '&IP:'
      FocusControl = EditIP
    end
    object LabelDIP: TLabel
      Left = 129
      Top = 11
      Width = 21
      Height = 13
      Caption = '&DIP:'
      FocusControl = EditDIP
    end
    object LabelSP: TLabel
      Left = 8
      Top = 43
      Width = 16
      Height = 13
      Caption = '&SP:'
      FocusControl = EditSP
    end
    object Label3: TLabel
      Left = 136
      Top = 43
      Width = 18
      Height = 13
      Caption = '&MP:'
      FocusControl = EditMP
    end
    object LabelRA: TLabel
      Left = 8
      Top = 74
      Width = 18
      Height = 13
      Caption = 'R&A:'
      FocusControl = EditRA
    end
    object LabelRB: TLabel
      Left = 136
      Top = 74
      Width = 17
      Height = 13
      Caption = 'R&B:'
      FocusControl = EditRB
    end
    object LabelRC: TLabel
      Left = 8
      Top = 107
      Width = 18
      Height = 13
      Caption = 'R&C:'
      FocusControl = EditRC
    end
    object LabelRD: TLabel
      Left = 136
      Top = 107
      Width = 18
      Height = 13
      Caption = 'R&D:'
      FocusControl = EditRD
    end
    object EditIP: TEdit
      Left = 28
      Top = 8
      Width = 81
      Height = 21
      TabOrder = 0
      Text = '00000000'
    end
    object EditDIP: TEdit
      Left = 156
      Top = 8
      Width = 81
      Height = 21
      TabOrder = 1
      Text = '00000000'
    end
    object EditSP: TEdit
      Left = 28
      Top = 40
      Width = 81
      Height = 21
      TabOrder = 2
      Text = '00000000'
    end
    object EditMP: TEdit
      Left = 156
      Top = 40
      Width = 81
      Height = 21
      TabOrder = 3
      Text = '00000000'
    end
    object EditRA: TEdit
      Left = 28
      Top = 71
      Width = 81
      Height = 21
      TabOrder = 4
      Text = '00000000'
    end
    object EditRB: TEdit
      Left = 156
      Top = 71
      Width = 81
      Height = 21
      TabOrder = 5
      Text = '00000000'
    end
    object EditRC: TEdit
      Left = 28
      Top = 104
      Width = 81
      Height = 21
      TabOrder = 6
      Text = '00000000'
    end
    object EditRD: TEdit
      Left = 156
      Top = 104
      Width = 81
      Height = 21
      TabOrder = 7
      Text = '00000000'
    end
  end
end
