#include "colors.inc"
#include "textures.inc" 
#include "functions.inc" 

global_settings { max_trace_level 8 
                  charset utf8  

                  }



background {0.25*Red}

light_source{ <-1500,1500,-1000> color White} 

camera{
    location <0, 0, -12>
    look_at <0,0,0> 
    right x*image_width/image_height
    angle 12
  } 


cone{-x,1,x,0
texture{pigment{Red}}
} 