unit RegerUnit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, ExtCtrls, StdCtrls, ComCtrls, ToolWin,
  Reper, Repernp, ImgList;

type
  TRegForm = class(TForm)
    MainPanel: TPanel;
    RegsGrid: TStringGrid;
    LabelReg: TLabel;
    LabelVal: TLabel;
    LabelStatus: TLabel;
    LabelState: TLabel;
    TopPanel: TPanel;
    ToolBar1: TToolBar;
    StepButton: TToolButton;
    BackButton: TToolButton;
    NanoprGrid: TStringGrid;
    nStepButton: TToolButton;
    nBackButton: TToolButton;
    LabelNanoPr: TLabel;
    ImageList1: TImageList;
    RunButton: TToolButton;
    NurButton: TToolButton;
    StopButton: TToolButton;
    ImageList0: TImageList;
    procedure FormCreate(Sender: TObject);
    procedure RegsGridGetEditMask(Sender: TObject; ACol, ARow: Integer;
      var Value: string);
    procedure RegsGridKeyPress(Sender: TObject; var Key: Char);
    procedure RegsGridGetEditText(Sender: TObject; ACol, ARow: Integer;
      var Value: string);
    procedure RegsGridSetEditText(Sender: TObject; ACol, ARow: Integer;
      const Value: string);
    procedure RegsGridSelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure RegsGridExit(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure StepButtonClick(Sender: TObject);
    procedure BackButtonClick(Sender: TObject);
    procedure nStepButtonClick(Sender: TObject);
    procedure nBackButtonClick(Sender: TObject);
    procedure RunButtonClick(Sender: TObject);
    procedure NurButtonClick(Sender: TObject);
    procedure StopButtonClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    RCPU : TRPU;
    CurReg : Integer;
    Running : Boolean;
    procedure ShowRegs;
    function SetReg(N : integer; value : string) : boolean;
    procedure ShowNanopr;
    procedure CheckStep(dir : boolean);
    procedure SetBtnRunning(Runs : boolean);
  end;

var
  RegForm: TRegForm;



implementation

{$R *.dfm}

uses DatadUnit, RevMain;

procedure TRegForm.BackButtonClick(Sender: TObject);
begin
 { TODO -oAlex -cNanoprogs : Consider nIp <>0 }
 RCPU.InvStep;
 ShowRegs;
 DataForm.DataGrid.Repaint;
 DataForm.StackGrid.Repaint;
end;

procedure TRegForm.CheckStep(dir: boolean);
begin
  Application.ProcessMessages;
  with DataForm do
   if CheckBrkAddr(RCPU.RR[RegIP])
   then Running := False;
   
  
end;

procedure TRegForm.FormCreate(Sender: TObject);
begin
  RCPU := TRPUNP.Create($1000000,$100000,'reper.np');
  ShowRegs;
  CurReg := 1;
  Running := False;
end;

procedure TRegForm.FormDestroy(Sender: TObject);
begin
 RCPU.Destroy;
end;

procedure TRegForm.nBackButtonClick(Sender: TObject);
begin
 RCPU.InvNanoStep;
 ShowRegs;
 ShowNanopr;
 DataForm.DataGrid.Repaint;
 DataForm.StackGrid.Repaint;
end;

procedure TRegForm.nStepButtonClick(Sender: TObject);
begin
 RCPU.NanoStep;
 ShowRegs;
 ShowNanopr;
 DataForm.DataGrid.Repaint;
 DataForm.StackGrid.Repaint;
end;

procedure TRegForm.NurButtonClick(Sender: TObject);
begin
 SetBtnRunning(True);
 Running := True;
 repeat
  RCPU.InvStep;
  CheckStep(False);
 until not Running;
 SetBtnRunning(False);
 ShowRegs;
 DataForm.DataGrid.Repaint;
 DataForm.StackGrid.Repaint;
end;

procedure TRegForm.RegsGridExit(Sender: TObject);
begin
  if not SetReg(CurReg,RegsGrid.Cells[1,CurReg])
  then RegsGrid.Cells[1,CurReg] := IntToHex(RCPU.RR[TRegs(CurReg)],8)
end;

procedure TRegForm.RegsGridGetEditMask(Sender: TObject; ACol, ARow: Integer;
  var Value: string);
begin
 Value := 'AAAAAAAA'
end;

procedure TRegForm.RegsGridGetEditText(Sender: TObject; ACol, ARow: Integer;
  var Value: string);
begin
 if ACol=2  then
 begin
  CurReg := ARow;
  LabelStatus.Caption := '->'+ Value
 end;
end;

procedure TRegForm.RegsGridKeyPress(Sender: TObject; var Key: Char);
begin
 if Key = #13  then
 begin
  if not SetReg(CurReg,RegsGrid.Cells[1,CurReg])
  then RegsGrid.Cells[1,CurReg] := IntToHex(RCPU.RR[TRegs(CurReg)],8)
 end
 else
 if Key in ['a'..'f'] then Key := UpCase(Key)
 else
  if not (Key in ['0'..'9','A'..'F',#8])
  then  Key := #0

end;

procedure TRegForm.RegsGridSelectCell(Sender: TObject; ACol, ARow: Integer;
  var CanSelect: Boolean);
begin
  CanSelect := True;
  if ACol = 1 then
  begin
   //if CurReg <> ARow then

   if SetReg(CurReg,RegsGrid.Cells[1,CurReg])
   then CanSelect := True
   else RegsGrid.Cells[1,CurReg] := IntToHex(RCPU.RR[TRegs(CurReg)],8);
   CurReg := ARow;
   LabelStatus.Caption := Format('Select %d',[ARow])
  end;

end;

procedure TRegForm.RegsGridSetEditText(Sender: TObject; ACol, ARow: Integer;
  const Value: string);
begin
 if ACol = 1 then
 begin
  SetReg(CurReg,RegsGrid.Cells[1,CurReg]);
  CurReg := ARow;
  LabelStatus.Caption := Value
 end;
end;

procedure TRegForm.RunButtonClick(Sender: TObject);
begin
 SetBtnRunning(True);
 Running := True;
 repeat
  RCPU.Step;
  CheckStep(True);
 until not Running;
 SetBtnRunning(False);
 ShowRegs;
 DataForm.DataGrid.Repaint;
 DataForm.StackGrid.Repaint;
end;

procedure TRegForm.SetBtnRunning(Runs: boolean);
begin
 StepButton.Enabled := not Runs;
 BackButton.Enabled := not Runs;
 nStepButton.Enabled := not Runs;
 nBackButton.Enabled := not Runs;
 RunButton.Enabled :=  not Runs;
 NurButton.Enabled := not Runs;
 StopButton.Enabled := Runs;
end;

function TRegForm.SetReg(N: integer; value: string): boolean;
var
 R,C : integer;
begin
  Result := false;
  if N >=0 then
  begin
    LabelState.Caption := Format(' Set R%d to %s',[N,value]);
    Val('$'+value,R,C);
    if C = 0 then
    begin
      Result := true;
      if R <> RCPU.RR[TRegs(CurReg)] then
      begin
        RCPU.RR[TRegs(CurReg)] := R;
        DataForm.DataGrid.Repaint;
        DataForm.StackGrid.Repaint;
        LabelState.Caption := LabelState.Caption + ', done'
      end;

    end;
  end;
end;

procedure TRegForm.ShowNanopr;
var
 i : integer;
begin
 with NanoprGrid,RCPU do
 begin
  LabelNanoPr.Caption := 'nIP = '+IntToStr(nIP);
  {- For simplicity -}
  { TODO -oAlex -cNanoprogs : Implement disabled buttons }
  StepButton.Enabled := (nIP=0);
  BackButton.Enabled := (nIP=0);
  RunButton.Enabled := (nIP=0);
  NurButton.Enabled := (nIP=0);
  {-                -}
  if nIP = 0 then
  begin
   RowCount := 8;
   for i := 0 to RowCount - 1 do
    Cells[1,i] := '';
   Row := 0;
  end
  else
  with NanoProgs[Comm] do
  begin
   RowCount := Length(RevCodes)+1;
   for i := 0 to RowCount - 2 do
    Cells[1,i] := RevOps[RevCodes[i]].OpCode;
   Cells[1,RowCount - 1] := '-FIN-'
  end;
  for i := 0 to RowCount - 1 do
  if i = nIP-1 then
  begin
   Cells[0,i] := '>';
   Row := i;
  end
  else
   Cells[0,i] := ''
 end;
end;

procedure TRegForm.ShowRegs;
var
 i : TRegs;
begin
 with RegsGrid do
  begin
    for i := TRegs(0) to TRegs(7) do
    begin
      Cells[0,ord(i){+1}] := sRegs[i];
      Cells[1,ord(i){+1}] := IntToHex(RCPU.RR[i],8);
    end;
  end;
 with MainForm.AnimateLogo do
 if FileExists(FileName) then
  Active := RegForm.RCPU.RR[RegDIP] <> 0
end;

procedure TRegForm.StepButtonClick(Sender: TObject);
begin
 { TODO -oAlex -cNanoprogs : Consider nIp <>0 }
 RCPU.Step;
 ShowRegs;
 DataForm.DataGrid.Repaint;
 DataForm.StackGrid.Repaint;
end;

procedure TRegForm.StopButtonClick(Sender: TObject);
begin
 Running := False;
end;

end.
