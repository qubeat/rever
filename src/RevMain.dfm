object MainForm: TMainForm
  Left = 21
  Top = 43
  BorderIcons = [biSystemMenu, biMinimize]
  Caption = 'ReveR'
  ClientHeight = 63
  ClientWidth = 349
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  Position = poDesigned
  PixelsPerInch = 96
  TextHeight = 13
  object AnimateLogo: TAnimate
    Left = 309
    Top = 0
    Width = 40
    Height = 63
    Align = alRight
    FileName = 'ReveR4.avi'
    StopFrame = 100
    ExplicitHeight = 44
  end
  object MainMenu1: TMainMenu
    Left = 224
    Top = 16
    object File1: TMenuItem
      Caption = '&File'
      object New1: TMenuItem
        Action = FileNew1
      end
      object Open1: TMenuItem
        Action = FileOpen1
      end
      object Save1: TMenuItem
        Caption = '&Save'
        Enabled = False
      end
      object SaveAs1: TMenuItem
        Action = FileSaveAs1
      end
      object N2: TMenuItem
        Caption = '-'
      end
      object Print1: TMenuItem
        Caption = '&Print...'
        Enabled = False
      end
      object PrintSetup1: TMenuItem
        Caption = 'P&rint Setup...'
        Enabled = False
      end
      object N1: TMenuItem
        Caption = '-'
      end
      object Exit1: TMenuItem
        Action = FileExit1
      end
    end
    object Debug1: TMenuItem
      Caption = '&Debug'
      OnClick = Debug1Click
      object Registers1: TMenuItem
        Action = ActRegisters
        AutoCheck = True
      end
      object Data1: TMenuItem
        Action = ActData
        AutoCheck = True
      end
    end
    object Help1: TMenuItem
      Caption = '&Help'
      object About1: TMenuItem
        Action = HelpAbout1
      end
    end
  end
  object ActionList1: TActionList
    Left = 264
    Top = 16
    object FileExit1: TFileExit
      Category = 'File'
      Caption = 'E&xit'
      Hint = 'Exit|Quits the application'
      ImageIndex = 43
    end
    object ActRegisters: TAction
      AutoCheck = True
      Caption = '&Registers'
      OnExecute = ActRegistersExecute
      OnUpdate = Debug1Click
    end
    object ActData: TAction
      AutoCheck = True
      Caption = '&Data'
      OnExecute = ActDataExecute
      OnUpdate = Debug1Click
    end
    object HelpAbout1: TAction
      Category = 'Help'
      Caption = '&About...'
      OnExecute = HelpAbout1Execute
    end
    object FileOpen1: TFileOpen
      Category = 'File'
      Caption = '&Open...'
      Dialog.DefaultExt = 'rmr'
      Dialog.Filter = 'ReveR Memory Dump (*.rmr)|*.rmr|ReveR Assembler  (*.rsr)|*.rsr'
      Dialog.InitialDir = '.'
      Dialog.Title = 'Load Codes'
      Hint = 'Open|Opens an existing file'
      ImageIndex = 7
      ShortCut = 16463
      OnAccept = FileOpen1Accept
    end
    object FileSaveAs1: TFileSaveAs
      Category = 'File'
      Caption = 'Save &As...'
      Dialog.DefaultExt = 'rmr'
      Dialog.Filter = 'ReveR Memory Dump (*.rmr)|*.rmr'
      Dialog.InitialDir = '.'
      Hint = 'Save As|Saves the active file with a new name'
      ImageIndex = 30
      OnAccept = FileSaveAs1Accept
    end
    object FileNew1: TAction
      Category = 'File'
      Caption = '&New'
      OnExecute = FileNew1Execute
    end
  end
end
