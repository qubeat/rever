unit Rassar;

interface

uses SysUtils, Reper;

procedure LoadRasm(FileName : String; RPU : TRPU; var CP0 : Integer);

implementation

const
 maxlabs = 2048;

type
 alabs = array[0..maxlabs-1] of integer;

procedure LabOrNumExt(var S : String; var Num : Integer; var lab : boolean;
   var Code : integer);
begin
   lab := true;
   if S[1] = '@' then
    S := Copy(S,2,Length(S)-1)
   else
    lab := false;
    Val(S,Num,Code);
end;


procedure LabOrNum(S : String; var Num : Integer; var lab : boolean);
var
 Er : integer;
begin
  LabOrNumExt(S,Num,lab,Er);
  if Er > 0 then
    raise EConvertError.Create('Illegal number: '+S);
end;


procedure LoadLabs(FileName : String; CP : Integer; var labs : alabs);
var
 F : Text;
 S : string;
 Lab : boolean;
 n,Num : integer;
begin
 try
  Assign(F,FileName);
  Reset(F);
  while not Eof(F) do
  begin
    Readln(F,S);
    n := Pos(';',S);
    if n > 0 then
    begin
     S := Copy(S,1,n-1);
    end;
    S := TrimRight(S);
    if S <> '' then
     if S[1]<> ':' then inc(CP)
     else
     begin
       S := Copy(S,2,Length(S)-1);
       LabOrNum(S,Num,lab);
       if lab then labs[Num] := CP
       else CP := Num
     end;
   end;
 finally
  Close(F);
 end;
end;

procedure CalcLabExpr(S : string; var N : integer; var labs : alabs);
var
 lab : boolean;
 P : integer;
 DN,K : integer;
begin
 S := Trim(Copy(S,2,Length(S)-1));
 N := 0;
 repeat
  K := 1;
  if S[1] = '-' then
  begin
    K := -1;
    S := Trim(Copy(S,2,Length(S)-1));
  end
  else
   if S[1] = '+' then
    S := Trim(Copy(S,2,Length(S)-1));
  LabOrNumExt(S,DN,lab,P);
  if lab then
   N := N+K*labs[DN]
  else
   N := N+K*DN;
   S := Trim(Copy(S,P,Length(S)-P+1));
 until (P=0) or (S='');
end;

procedure LoadRasm(FileName : String; RPU : TRPU; var CP0 : Integer);
var
 labs : alabs;
 F : Text;
 S : string;
 Lab : boolean;
 n,Num : integer;
begin
 LoadLabs(FileName, CP0, labs);
 try
  Assign(F,FileName);
  Reset(F);
  while not Eof(F) do
  begin
    Readln(F,S);
    n := Pos(';',S);
    if n > 0 then
    begin
     S := Copy(S,1,n-1);
    end;
    S := TrimRight(S);
    if S <> '' then
     if S[1] <> ':' then
     begin
      labs[0] := CP0;
      if S[1] = '#' then
       CalcLabExpr(S,Num,Labs)
      else
       RPU.FindInstr(S,Num);
      RPU.MeM[CP0] := Num;
      inc(CP0)
     end
     else
     begin
       S := Copy(S,2,Length(S)-1);
       LabOrNum(S,Num,lab);
       if lab then labs[Num] := CP0
       else CP0 := Num
     end;
   end;
  finally
  Close(F);
 end;
end;


end.
