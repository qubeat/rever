program MasaM;

{$APPTYPE CONSOLE}

uses
  SysUtils,
  Classes,
  MsM in 'MsM.pas';

var
 M,L1,L2,L3 : TMLines;
 F : Text;
begin
 Assign(F,'test.msm');
 reset(F);
 L1 := TMLines.Create;
 L2 := TMLines.Create;
 L3 := TMLines.Create;
 L1.LoadFromFile(F);
 L2.LoadFromFile(F);
 L3.LoadFromFile(F);
 close(F);

 M := TMLines.Create;
 M.JoinWithArgs(L1,[L2,L3]);
 Assign(F,'testmsm.rsr');
 rewrite(F);
 M.SaveToFile(F);
 Close(F)
end.
