unit Reper;

interface


uses SysUtils, Classes, Dialogs;

type

TRegs = (RegCM,RegIP,RegDIP,RegSP,RegMP,RegRA,RegRB,RegRC);

PRPU = ^TRPU;

TROp = class(TObject)  {elementary reversible operation}
  IDX : integer;
  INVIDX : integer;
  OpCode : String;
  procedure perform(PR : PRPU); virtual; abstract;
end;

TNanops = class(TObject)
 IDX : integer;
 Mnemo : String;
 ArCodes : array of integer;
 constructor Create(Num : Integer; Name : String; Codes : array of integer);
 destructor Destroy; override;
 function OpLen : integer; virtual;
end;

TNanops2 = class(TNanops)
 function OpLen : integer; override;
end;


TRPU = class(TObject)     {Reversible Processing Unit}
 RR : array[TRegs] of integer;
 MeM : array of integer;
 NanoProgs : array of TNanops; {Array of nanoprograms}
 ArOps : array of TROp; {Array of ops}
 constructor Create(ATotMem : integer);
 destructor Destroy; override;
 function GetTotMem : integer;
 property TotMem : integer read GetTotMem;
 procedure Step;
 procedure InvStep;
end;


TRNOP = class(TRop)
  constructor Create;
  procedure perform(PR : PRPU); override;
end;

TRINCA = class(TRop)
  constructor Create;
  procedure perform(PR : PRPU); override;
end;

TRDECA = class(TRop)
  constructor Create;
  procedure perform(PR : PRPU); override;
end;

TRSWPB = class(TRop)
  constructor Create;
  procedure perform(PR : PRPU); override;
end;

TRSWPC = class(TRop)
  constructor Create;
  procedure perform(PR : PRPU); override;
end;

TRSWPDIP = class(TRop)
  constructor Create;
  procedure perform(PR : PRPU); override;
end;

TRSWPSP = class(TRop)
  constructor Create;
  procedure perform(PR : PRPU); override;
end;

TRSWPMP = class(TRop)
  constructor Create;
  procedure perform(PR : PRPU); override;
end;


TRSWPMEM = class(TRop)
  constructor Create;
  procedure perform(PR : PRPU); override;
end;

TRADD = class(TRop)
  constructor Create;
  procedure perform(PR : PRPU); override;
end;

TRSUB = class(TRop)
  constructor Create;
  procedure perform(PR : PRPU); override;
end;

TRADDNX = class(TRop)
  constructor Create;
  procedure perform(PR : PRPU); override;
end;

TRSUBNX = class(TRop)
  constructor Create;
  procedure perform(PR : PRPU); override;
end;


TRADDIP = class(TRop)
  constructor Create;
  procedure perform(PR : PRPU); override;
end;

TRSUBIP = class(TRop)
  constructor Create;
  procedure perform(PR : PRPU); override;
end;

TRNEGA = class(TRop)
  constructor Create;
  procedure perform(PR : PRPU); override;
end;


const

 sRegs : array[TRegs] of string =
  ('CM','IP','DIP','SP','MP','RA','RB','RC');

 OpNOP    = 0;  {NOP}
 OpINCA =   1;  {RA <- RA + 1}
 OpDECA =   2;  {RA <- RA - 1}
 OpSWPB =   3;  {RA <-> RB}
 OpSWPC =   4;  {RA <-> RC}
 OpSWPDIP = 5;  {RA <-> DIP}
 OpSWPSP =  6;  {RA <-> SP}
 OpSWPMP =  7;  {RA <-> MP}
 OpSWPMEM = 8;  {RA <-> MeM[MP]}
 OpADD =    9;  {RA <- RA + RB}
 OpSUB =   10;  {RA <- RA - RB}
 OpADDNX = 11;  {RA <- RA + [IP+1]}
 OpSUBNX = 12;  {RA <- RA - [IP+1]}
 OpADDIP = 13;  {RA <- RA + IP}
 OpSUBIP = 14;  {RA <- RA - IP}
 OpNEGA =  15;  {RA <- -RA}

 NumOp = 16;




 NpNop = 0;    {Nop}
 NpIncDIP = 1; {DIP <- DIP + 1}
 NpDecDIP = 2; {DIP <- DIP - 1}

 NpStartOp = 2;
 NumNanoPr = NpStartOp+NumOp;


implementation

{ RCPU }

constructor TRPU.Create(ATotMem : integer);
var
 i : integer;
begin
  inherited Create;
  SetLength(MeM,ATotMem);
  RR[RegCM] := 0;  {0-CM}
  RR[RegIP] := 0;  {1-IP}
  RR[RegDIP] := 0; {2-DIP}
  RR[RegSP] := ATotMem-1;{3-SP}
  RR[RegMP] := 0;  {4-MP}
  RR[RegRA] := 0;  {5-RA}
  RR[RegRB] := 0;  {6-RB}
  RR[RegRC] := 0;  {7-RC}
  SetLength(ArOps,NumOp);
  ArOPs[OpNOP] := TRNOP.Create;
  ArOPs[OpINCA] := TRINCA.Create;
  ArOPs[OpDECA] := TRDECA.Create;
  ArOPs[OpSWPB] := TRSWPB.Create;
  ArOPs[OpSWPC] := TRSWPC.Create;
  ArOPs[OpSWPDIP] := TRSWPDIP.Create;
  ArOPs[OpSWPSP] := TRSWPSP.Create;
  ArOPs[OpSWPMP] := TRSWPMP.Create;
  ArOPs[OpSWPMEM] := TRSWPMEM.Create;
  ArOPs[OpADD] := TRADD.Create;
  ArOPs[OpSUB] := TRSUB.Create;
  ArOPs[OpADDNX] := TRADDNX.Create;
  ArOPs[OpSUBNX] := TRSUBNX.Create;
  ArOPs[OpADDIP] := TRADDIP.Create;
  ArOPs[OpSUBIP] := TRSUBIP.Create;
  ArOPs[OpNEGA] := TRNEGA.Create;
  for i := 0 to Length(ArOPs) - 1 do
    if (ArOPs[i].IDX <> i) or
      (ArOPs[ArOPs[i].INVIDX].INVIDX <> i)
      then ShowMessage('Error in opcodes initialization');
  SetLength(NanoProgs,NumNanoPr);
  NanoProgs[NpNop] := TNanops.Create(NpNop,'Nop',[OpNOP]);
  NanoProgs[NpIncDIP] := TNanops.Create(NpIncDIP,
             'Inc DIP',[OpSWPDIP,OpINCA,OpSWPDIP]);
  NanoProgs[NpDecDIP] := TNanops.Create(NpDecDIP,
             'Dec DIP',[OpSWPDIP,OpDECA,OpSWPDIP]);
  for i := 1 to NumOp - 1 do
   NanoProgs[NpStartOp+i] := TNanops.Create(NpStartOp+i,
             ArOps[i].OpCode,[i]);

  for i := 0 to Length(NanoProgs) - 1 do
   if NanoProgs[i].IDX <> i
      then ShowMessage('Error in nanoprogs initialization');

end;

destructor TRPU.Destroy;
var
 i : integer;
begin
  for i := 0 to Length(NanoProgs)-1 do
    NanoProgs[i].Free;
  NanoProgs := nil;
  for i := 0 to Length(ArOps)-1 do
    ArOps[i].Free;
  ArOps := nil;
  inherited;
end;

function TRPU.GetTotMem: integer;
begin
 GetTotMem := Length(Mem)
end;

procedure TRPU.InvStep;
var
 i : integer;
begin
 RR[RegIP] := RR[RegIP] - RR[RegDIP];
 RR[RegCM] := RR[RegCM] + MeM[RR[RegIP]];
 with NanoProgs[RR[RegCM]] do
 for i := Length(ArCodes) - 1 downto 0 do
  ArOps[ArOps[ArCodes[i]].INVIDX].perform(@Self);
 RR[RegCM] := RR[RegCM] - MeM[RR[RegIP]];
end;


procedure TRPU.Step;
var
 i : integer;
begin
 RR[RegCM] := RR[RegCM] + MeM[RR[RegIP]];
 with NanoProgs[RR[RegCM]] do
 for i := 0 to Length(ArCodes) - 1 do
  ArOps[ArCodes[i]].perform(@Self);
 RR[RegCM] := RR[RegCM] - MeM[RR[RegIP]];
 RR[RegIP] := RR[RegIP] + RR[RegDIP];
end;

{ TNanops }

constructor TNanops.Create(Num: Integer; Name: String; Codes: array of integer);
var
 i : integer;
begin
 inherited Create;
 IDX := Num;
 Mnemo := Name;
 SetLength(ArCodes,Length(Codes));
 for i := 0 to Length(Codes) - 1 do
   ArCodes[i] := Codes[i]
end;

destructor TNanops.Destroy;
begin
  ArCodes := nil;
  inherited;
end;

function TNanops.OpLen: integer;
begin
 OpLen := 1;
end;

{ TNanops2 }

function TNanops2.OpLen: integer;
begin
 OpLen := 2;
end;


{ TRNOP }

constructor TRNOP.Create;
begin
 inherited Create;
 IDX := OpNOP;
 INVIDX := OpNOP;
 OpCode := 'NOP';
end;

procedure TRNOP.perform(PR : PRPU);
begin
 {NO Operation}
end;


{ TRINCA }

constructor TRINCA.Create;
begin
 inherited Create;
 IDX := OpINCA;
 INVIDX := OpDECA;
 OpCode := 'INC RA';
end;

procedure TRINCA.perform(PR : PRPU);
begin
 Inc(PR^.RR[RegRA])
 {RA <- RA + 1}
end;

{ TRDECA }

constructor TRDECA.Create;
begin
 inherited Create;
 IDX := OpDECA;
 INVIDX := OpINCA;
 OpCode := 'DEC RA';
end;

procedure TRDECA.perform(PR: PRPU);
begin
 Dec(PR^.RR[RegRA])
 {RA <- RA - 1}
end;

{ TRSWPB }

constructor TRSWPB.Create;
begin
 inherited Create;
 IDX := OpSWPB;
 INVIDX := OpSWPB;
 OpCode := 'SWP RB';
end;

procedure TRSWPB.perform(PR: PRPU);
var
 M : integer;
begin
 with PR^ do
 begin
  M := RR[RegRA];
  RR[RegRA] := RR[RegRB];
  RR[RegRB] := M
 end;
 {RA <-> RB}
end;

{ TRSWPC }

constructor TRSWPC.Create;
begin
 inherited Create;
 IDX := OpSWPC;
 INVIDX := OpSWPC;
 OpCode := 'SWP RC';
end;

procedure TRSWPC.perform(PR: PRPU);
var
 M : integer;
begin
 with PR^ do
 begin
  M := RR[RegRA];
  RR[RegRA] := RR[RegRC];
  RR[RegRC] := M
 end;
 {RA <-> RC}
end;

{ TRSWPDIP }

constructor TRSWPDIP.Create;
begin
 inherited Create;
 IDX := OpSWPDIP;
 INVIDX := OpSWPDIP;
 OpCode := 'SWP DIP';
end;

procedure TRSWPDIP.perform(PR: PRPU);
var
 M : integer;
begin
 with PR^ do
 begin
  M := RR[RegRA];
  RR[RegRA] := RR[RegDIP];
  RR[RegDIP] := M
 end;
 {RA <-> DIP}
end;

{ TSWPSP }

constructor TRSWPSP.Create;
begin
 inherited Create;
 IDX := OpSWPSP;
 INVIDX := OpSWPSP;
 OpCode := 'SWP SP';
end;

procedure TRSWPSP.perform(PR: PRPU);
var
 M : integer;
begin
 with PR^ do
 begin
  M := RR[RegRA];
  RR[RegRA] := RR[RegSP];
  RR[RegSP] := M
 end;
 {RA <-> SP}
end;

{ TSWPMP }

constructor TRSWPMP.Create;
begin
 inherited Create;
 IDX := OpSWPMP;
 INVIDX := OpSWPMP;
 OpCode := 'SWP MP';
end;

procedure TRSWPMP.perform(PR: PRPU);
var
 M : integer;
begin
 with PR^ do
 begin
  M := RR[RegRA];
  RR[RegRA] := RR[RegMP];
  RR[RegMP] := M
 end;
 {RA <-> MP}
end;



{ TRSWPMEM }

constructor TRSWPMEM.Create;
begin
 inherited Create;
 IDX := OpSWPMEM;
 INVIDX := OpSWPMEM;
 OpCode := 'SWP MEM';
end;

procedure TRSWPMEM.perform(PR: PRPU);
var
 M : integer;
begin
 with PR^ do
 begin
  M := RR[RegRA];
  RR[RegRA] := MeM[RR[RegMP]];
  MeM[RR[RegMP]] := M
 end;
 {RA <-> MeM[MP]}
end;


{ TRADD }

constructor TRADD.Create;
begin
 inherited Create;
 IDX := OpADD;
 INVIDX := OpSUB;
 OpCode := 'ADD';
end;

procedure TRADD.perform(PR: PRPU);
begin
 with PR^ do
  RR[RegRA] := RR[RegRA] + RR[RegRB]
 { RA <- RA + RB }
end;

{ TRSUB }

constructor TRSUB.Create;
begin
 inherited Create;
 IDX := OpSUB;
 INVIDX := OpADD;
 OpCode := 'SUB';
end;

procedure TRSUB.perform(PR: PRPU);
begin
 with PR^ do
  RR[RegRA] := RR[RegRA] - RR[RegRB]
 { RA <- RA - RB }
end;

{ TRADDNX }

constructor TRADDNX.Create;
begin
 inherited Create;
 IDX := OpADDNX;
 INVIDX := OpSUBNX;
 OpCode := 'ADD NX';
end;

procedure TRADDNX.perform(PR: PRPU);
begin
 with PR^ do
  RR[RegRA] := RR[RegRA] + MeM[RR[RegIP]+1]
 { RA <- RA + MeM[IP+1] }
end;

{ TRSUBNX }

constructor TRSUBNX.Create;
begin
 inherited Create;
 IDX := OpSUBNX;
 INVIDX := OpADDNX;
 OpCode := 'SUB NX';
end;

procedure TRSUBNX.perform(PR: PRPU);
begin
 with PR^ do
  RR[RegRA] := RR[RegRA] - MeM[RR[RegIP]+1]
 { RA <- RA - MeM[IP+1] }
end;


{ TRADDIP }

constructor TRADDIP.Create;
begin
 inherited Create;
 IDX := OpADDIP;
 INVIDX := OpSUBIP;
 OpCode := 'ADD IP';
end;

procedure TRADDIP.perform(PR: PRPU);
begin
 with PR^ do
  RR[RegRA] := RR[RegRA] + RR[RegIP]
 { RA <- RA + IP }
end;

{ TRSUBIP }

constructor TRSUBIP.Create;
begin
inherited Create;
 IDX := OpSUBIP;
 INVIDX := OpADDIP;
 OpCode := 'SUB IP';
end;

procedure TRSUBIP.perform(PR: PRPU);
begin
with PR^ do
  RR[RegRA] := RR[RegRA] - RR[RegIP]
 { RA <- RA - IP }
end;

{ TRNEGA }

constructor TRNEGA.Create;
begin
 inherited Create;
 IDX := OpNEGA;
 INVIDX := OpNEGA;
 OpCode := 'NEG RA';
end;

procedure TRNEGA.perform(PR: PRPU);
begin
 with PR^ do
  RR[RegRA] := -RR[RegRA]
 { RA <- -RA }
end;

end.
