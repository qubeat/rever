#include "colors.inc"
#include "textures.inc" 
#include "functions.inc" 

global_settings { max_trace_level 8 
                  charset utf8  

                  }



background {0.25*Green}

light_source{ <-1500,1500,-1000> color White} 

camera{
    location <0, 0, -12>
    look_at <0,0,0> 
    right x*image_width/image_height
    angle 12
  } 

union{
cone{-x,0.9,0,0} 
cone{0,0.6,x,0}
cylinder{0.8*x,x,0.5}    
//scale <-1,1,1>
texture{pigment{Green}}
} 