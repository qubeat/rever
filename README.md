# ReveR - reversible processor with stack

![screenshot windows](https://bytebucket.org/qubeat/rever/raw/4c3563753e0ee54f14532f069eb7e42196da2ac8/src/screenshot_winxp.png?token=05d7ff5ca9b1623a936b3c45385f696949e02a20)

## Delphi source. Version 1.0.0.3

Description may be found in e-print
http://arxiv.org/abs/1104.0924

Compiled Windows binaries: [BitBucket](https://bitbucket.org/qubeat/rever/downloads/ReveR_1003-20110601.zip) or [OneDrive](https://onedrive.live.com/redir?resid=E3BFDBAAA0966790!105&authkey=!ebXaqXzcFuQ%24&ithint=folder%2czip)
